var tetrisArray = [
    [ // Tetrimino 1 L
        [   [ -1, 0 ],  [ 0, 0 ],   [ 1, 0 ],   [ 1, 1 ]    ], // rotation 1
        [   [ 0, -1 ],  [ 0, 0 ],   [ 0, 1 ],   [ 1, -1 ]   ], // rotation 2
        [   [ -1, -1 ], [ -1, 0 ],  [ 0, 0 ],   [ 1, 0 ]    ], // rotation 3
        [   [ -1, 1 ],  [ 0, -1 ],  [ 0, 0 ],   [ 0, 1 ]    ], // rotation 4
        [ 1, 0.5, 0 ], // color r, g, b
        new THREE.TextureLoader().load( "texture/block-1.png", function() {
            tetrisArray[ 0 ][ 5 ].magFilter = THREE.NearestFilter;
            tetrisArray[ 0 ][ 5 ].minFilter = THREE.NearestFilter;
        } ) // Texture
    ],
    [ // Tetrimino 2 J
        [   [ -1, 0 ],  [ -1, 1 ],  [ 0, 0 ],   [ 1, 0 ]    ], // rotation 1
        [   [ 0, -1 ],  [ 0, 0 ],   [ 0, 1 ],   [ 1, 1 ]    ], // rotation 2
        [   [ -1, 0 ],  [ 0, 0 ],   [ 1, 0 ],   [ 1, -1 ]   ], // rotation 3
        [   [ -1, -1 ], [ 0, -1 ],  [ 0, 0 ],   [ 0, 1 ]    ], // rotation 4
        [ 0, 0, 1 ], // color r, g, b
        new THREE.TextureLoader().load( "texture/block-2.png", function() {
            tetrisArray[ 1 ][ 5 ].magFilter = THREE.NearestFilter;
            tetrisArray[ 1 ][ 5 ].minFilter = THREE.NearestFilter;
        } ) // Texture
    ],
    [ // Tetrimino 3 O
        [   [ -1, 0 ],  [ -1, 1 ],  [ 0, 0 ],   [ 0, 1 ]    ], // rotation 1
        [   [ -1, 0 ],  [ -1, 1 ],  [ 0, 0 ],   [ 0, 1 ]    ], // rotation 2
        [   [ -1, 0 ],  [ -1, 1 ],  [ 0, 0 ],   [ 0, 1 ]    ], // rotation 3
        [   [ -1, 0 ],  [ -1, 1 ],  [ 0, 0 ],   [ 0, 1 ]    ], // rotation 4
        [ 1, 1, 0 ], // color r, g, b
        new THREE.TextureLoader().load( "texture/block-3.png", function() {
            tetrisArray[ 2 ][ 5 ].magFilter = THREE.NearestFilter;
            tetrisArray[ 2 ][ 5 ].minFilter = THREE.NearestFilter;
        } ) // Texture
    ],
    [ // Tetrimino 4 S
        [   [ -1, 0 ],  [ 0, 0 ],   [ 0, 1 ],   [ 1, 1 ]    ], // rotation 1
        [   [ 0, 0 ],   [ 0, 1 ],   [ 1, -1 ],  [ 1, 0 ]    ], // rotation 2
        [   [ -1, -1 ], [ 0, -1 ],  [ 0, 0 ],   [ 1, 0 ]    ], // rotation 3
        [   [ -1, 0 ],  [ -1, 1 ],  [ 0, -1 ],  [ 0, 0 ]    ], // rotation 4
        [ 0, 1, 0 ], // color r, g, b
        new THREE.TextureLoader().load( "texture/block-4.png", function() {
            tetrisArray[ 3 ][ 5 ].magFilter = THREE.NearestFilter;
            tetrisArray[ 3 ][ 5 ].minFilter = THREE.NearestFilter;
        } ) // Texture
    ],
    [ // Tetrimino 5 Z
        [   [ -1, 1 ],  [ 0, 0 ],   [ 0, 1 ],   [ 1, 0 ]    ], // rotation 1
        [   [ 0, -1 ],  [ 0, 0 ],   [ 1, 0 ],   [ 1, 1 ]    ], // rotation 2
        [   [ -1, 0 ],  [ 0, -1 ],  [ 0, 0 ],   [ 1, -1 ]   ], // rotation 3
        [   [ -1, -1 ], [ -1, 0 ],  [ 0, 0 ],   [ 0, 1 ]    ], // rotation 4
        [ 1, 0, 0 ], // color r, g, b
        new THREE.TextureLoader().load( "texture/block-5.png", function() {
            tetrisArray[ 4 ][ 5 ].magFilter = THREE.NearestFilter;
            tetrisArray[ 4 ][ 5 ].minFilter = THREE.NearestFilter;
        } ) // Texture
    ],
    [ // Tetrimino 6 T
        [   [ -1, 0 ],  [ 0, 0 ],   [ 0, 1 ],  [ 1, 0 ]    ], // rotation 1
        [   [ 0, -1 ],  [ 0, 0 ],   [ 0, 1 ],  [ 1, 0 ]    ], // rotation 2
        [   [ -1, 0 ],  [ 0, -1 ],  [ 0, 0 ],   [ 1, 0 ]    ], // rotation 3
        [   [ -1, 0 ],  [ 0, -1 ],  [ 0, 0 ],   [ 0, 1 ]   ], // rotation 4
        [ 0.6, 0.1, 0.9 ], // color r, g, b
        new THREE.TextureLoader().load( "texture/block-6.png", function() {
            tetrisArray[ 5 ][ 5 ].magFilter = THREE.NearestFilter;
            tetrisArray[ 5 ][ 5 ].minFilter = THREE.NearestFilter;
        } ) // Texture
    ],
    [ // Tetrimino 7 I
        [   [ -1, 0 ],  [ 0, 0 ],   [ 1, 0 ],   [ 2, 0 ]    ], // rotation 1
        [   [ 0, 1 ],   [ 0, 0 ],   [ 0, -1 ],  [ 0, -2 ]   ], // rotation 2
        [   [ -1, 0 ],  [ 0, 0 ],   [ 1, 0 ],   [ 2, 0 ]    ], // rotation 3
        [   [ 0, 1 ],   [ 0, 0 ],   [ 0, -1 ],  [ 0, -2 ]   ], // rotation 4
        [ 0, 1, 1, ], // color r, g, b
        new THREE.TextureLoader().load( "texture/block-7.png", function() {
            tetrisArray[ 6 ][ 5 ].magFilter = THREE.NearestFilter;
            tetrisArray[ 6 ][ 5 ].minFilter = THREE.NearestFilter;
        } ) // Texture
    ]
];

var Tetris = function( tetrimino, moveWaitMsX, waitRotate ){
    /*
    var r = tetrisArray[ tetrimino ][ 4 ][ 0 ];
    var g = tetrisArray[ tetrimino ][ 4 ][ 1 ];
    var b = tetrisArray[ tetrimino ][ 4 ][ 2 ];
    
    this.block1 = createRectangle( score[0], 1, 1, 0, 0, r, g, b );
    this.block2 = createRectangle( score[0], 1, 1, 0, 0, r, g, b );
    this.block3 = createRectangle( score[0], 1, 1, 0, 0, r, g, b );
    this.block4 = createRectangle( score[0], 1, 1, 0, 0, r, g, b );
    */
    this.block1 = createRectangleTexture( 0, 0, tetrimino );
    this.block2 = createRectangleTexture( 0, 0, tetrimino );
    this.block3 = createRectangleTexture( 0, 0, tetrimino );
    this.block4 = createRectangleTexture( 0, 0, tetrimino );
    
    
    // Which tetriminos this one is.
    this.tetrimino = tetrimino;
    
    // "Speed" of tetris. Wait that long before next move.
    this.moveWaitMsX = moveWaitMsX;
    this.waitRotate = waitRotate;
    
    // How long it's been since last move
    this.lastMoveTimeX = moveWaitMsX;
    this.lastRotate = waitRotate;
    
    // Variable used to check current rotation
    // defaults to 1
    // 1 - 4 different rotations
    this.rotation = 1;
    this.lastRotation = 1;
    
    // Center position of tetris
    // Tetrimino starts from 5, -1 x, y
    this.posx = 5;
    this.posy = -1;
    
    this.setRotation( this.tetrimino, 0 );
}

Tetris.prototype.updateLastMoveTime = function( deltaTime ) {
    // Only update time if it's smaller than moveWaitMs
    // This prevents stacking lastMoveTimeX to some insane number.
    // Also we can now do this.lastMoveTimeX -= this.moveWaitMsX instead of this.lastMoveTimeX = 0
    // If the move is performed it will be "fair" moving without losing 10 or so ms every logic cycle
    // Carry extra times from the last move.
    if( this.lastMoveTimeX < this.moveWaitMsX ){
        //console.log( "update Time X" );
        this.lastMoveTimeX += deltaTime;
    }
    if( this.lastRotate < this.waitRotate ){
        this.lastRotate += deltaTime;
    }
}

// Moves x grid in x direction
Tetris.prototype.moveX = function( posX){
    // Performs the movement only if enough times have passed.
    if( !( this.movementCollisionX( posX ) ) ){
        if( this.lastMoveTimeX >= this.moveWaitMsX ){ 
            this.posx += posX;
            this.lastMoveTimeX -= this.moveWaitMsX;
            
            this.block1.position.x += posX
            this.block2.position.x += posX
            this.block3.position.x += posX
            this.block4.position.x += posX
        }
    }
}

Tetris.prototype.instantDrop = function(){
    var drop = 0;
    // Find lowest point without collision
    for( var y = -1; y < 22; y--){
        if( this.movementCollisionY( y ) ){
            break;
        }
        drop = y;
    }
    
    // Move to lowest point without collision
    this.posy += drop;
    
    this.block1.position.y += drop;
    this.block2.position.y += drop;
    this.block3.position.y += drop;
    this.block4.position.y += drop;
}

// Force move tetris
// Used by rotation. When outofbound after rotation it will forcefully move object
Tetris.prototype.moveXForce = function( offset ){
    this.posx += offset;
    
    this.block1.position.x += offset;
    this.block2.position.x += offset;
    this.block3.position.x += offset;
    this.block4.position.x += offset;
}

Tetris.prototype.moveYForce = function( offset ) {
    this.posy += offset;
    
    this.block1.position.y += offset;
    this.block2.position.y += offset;
    this.block3.position.y += offset;
    this.block4.position.y += offset;
}

// rotate tetris into new rotation
// -1 = left || 1 = right
Tetris.prototype.rotate = function( rotate ) {
    var rotationGraceTime = 0;
    if( this.lastRotate >= this.waitRotate ){
        this.lastRotate -= this.waitRotate;
        
        this.lastRotation = this.rotation;
        
        this.rotation += rotate;
        
        if( this.rotation > 4 ) {
            this.rotation = 1;
        }
        else if( this.rotation <  1 ) {
            this.rotation = 4;
        }
        
        rotationGraceTime = 60;
        
        // Do the following checks after every rotation
            // 1. Check if tetris is out of bound
            // TODO: 2. Check if tetris is overlapping
        switch ( this.rotation ) {
            case 1:
                this.setRotation( this.tetrimino, 0 );
                this.rotateOutofBoundCheck();
                // Double check out of bound for I tetrimino
                if( this.tetrimino ==  6 ){
                    this.rotateOutofBoundCheck();
                }
                this.rotationCollision();
                break;
            case 2:
                this.setRotation( this.tetrimino, 1 );
                this.rotateOutofBoundCheck();
                // Double check out of bound for I tetrimino
                if( this.tetrimino ==  6 ){
                    this.rotateOutofBoundCheck();
                }
                this.rotationCollision();
                break;
            case 3:
                this.setRotation( this.tetrimino, 2 );
                this.rotateOutofBoundCheck();
                // Double check out of bound for I tetrimino
                if( this.tetrimino ==  6 ){
                    this.rotateOutofBoundCheck();
                }
                this.rotationCollision();
                break;
            case 4:
                this.setRotation( this.tetrimino, 3 );
                this.rotateOutofBoundCheck();
                // Double check out of bound for I tetrimino
                if( this.tetrimino ==  6 ){
                    this.rotateOutofBoundCheck();
                }
                this.rotationCollision();
                break;
        }
    }
    
    return rotationGraceTime;
}

Tetris.prototype.rotateOutofBoundCheck = function() {
    if( this.outOfBoundXLeft( 0 ) ) {
        this.moveXForce( 1 );
    }
    else if ( this.outOfBoundXRight( 0 ) ){
        this.moveXForce( -1 );
    }
    if( 
        ( this.block1.position.y ) < -22 || // block1 bottom bound check
        ( this.block2.position.y ) < -22 || // block2 bottom bound check
        ( this.block3.position.y ) < -22 || // block3 bottom bound check
        ( this.block4.position.y ) < -22    // block4 bottom bound check
    ){
        this.moveYForce( 1 );
    }
}

// TODO: COLLISION
// updateGameGrid is performed after all movements are finished
// So technically our Tetris is still in last rotation
// So during this "temporary" rotation we can check and find right position to fit "temporary" rotation
Tetris.prototype.rotationCollision = function() {
    // First lets check if there's collision at all.
    // If not then do nothing
    if(
        gameGrid[ Math.abs( this.block1.position.y ) ][ this.block1.position.x ] != false ||
        gameGrid[ Math.abs( this.block2.position.y ) ][ this.block2.position.x ] != false ||
        gameGrid[ Math.abs( this.block3.position.y ) ][ this.block3.position.x ] != false ||
        gameGrid[ Math.abs( this.block4.position.y ) ][ this.block4.position.x ] != false 
    ){
        // Lets try finding good spot for our Tetris since there's collision
        // We can now use movementCollisionX/Y to find new spot
        // finding order range 1y 1x?
            // -1y -0x
        if( !( this.movementCollisionY( -1 ) ) ){
            this.moveYForce( -1 );
        }
            // -1y -1x
        else if( !( this.movementCollisionY( -1 ) || this.movementCollisionX( -1 ) ) ){
            this.moveYForce( -1 );
            this.moveXForce( -1 );
        }
            // -1y +1x
        else if( !( this.movementCollisionY( -1 ) || this.movementCollisionX( 1 ) ) ){
            this.moveYForce( -1 );
            this.moveXForce( 1 );
        }
            // +1y -0x
        else if( !( this.movementCollisionY( 1 ) ) ){
            this.moveYForce( 1 );
        }
            // -0y -1x
        else if( !( this.movementCollisionX( -1 ) ) ){
            this.moveXForce( -1 );
        }
            // -0y +1x
        else if( !( this.movementCollisionX( 1 ) ) ){
            this.moveXForce( 1 );
        }
            // +1y -1x
        else if( !( this.movementCollisionY( 1 ) || this.movementCollisionX( -1 ) ) ){
            this.moveYForce( 1 );
            this.moveXForce( -1 );
        }
            // +1y +1x
        else if( !( this.movementCollisionY( 1 ) || this.movementCollisionX( 1 ) ) ){
            this.moveYForce( 1 );
            this.moveXForce( 1 );
        }
            // +2y -0x
        else if( !( this.movementCollisionY( 2 ) ) ){
            this.moveYForce( 2 );
        }
        else{
            // Do -2x check for I block
            // it's blocks are [ -1, 0 ][ 0, 0 ][ 1, 0 ][ 2, 0 ]
            if( this.tetrimino ==  6 && !( this.movementCollisionX( -2 ) ) ){
                    this.moveXForce( -2 );
            }
            else{
                console.log( "wow... no place for this tetris..." );
                // reverse back to last rotation
                this.rotation = this.lastRotation;
                switch ( this.lastRotation ) {
                    case 1:
                        this.setRotation( this.tetrimino, 0 );
                        break;
                    case 2:
                        this.setRotation( this.tetrimino, 1 );
                        break;
                    case 3:
                        this.setRotation( this.tetrimino, 2 );
                        break;
                    case 4:
                        this.setRotation( this.tetrimino, 3 );
                        break;
                }
            }
        }
    }
}

Tetris.prototype.endTetrisLifeHere = function(){
    gameGrid[ Math.abs( this.block1.position.y ) ][ this.block1.position.x ] = this.block1;
    gameGrid[ Math.abs( this.block2.position.y ) ][ this.block2.position.x ] = this.block2;
    gameGrid[ Math.abs( this.block3.position.y ) ][ this.block3.position.x ] = this.block3;
    gameGrid[ Math.abs( this.block4.position.y ) ][ this.block4.position.x ] = this.block4;
}

Tetris.prototype.movementCollisionX = function( offset ){
    if( this.outOfBoundXLeft( offset ) || this.outOfBoundXRight( offset ) ){
        return true;
    }
    else{
        if(
            gameGrid[ Math.abs( this.block1.position.y ) ][ this.block1.position.x + offset ] != false ||
            gameGrid[ Math.abs( this.block2.position.y ) ][ this.block2.position.x + offset ] != false ||
            gameGrid[ Math.abs( this.block3.position.y ) ][ this.block3.position.x + offset ] != false ||
            gameGrid[ Math.abs( this.block4.position.y ) ][ this.block4.position.x + offset ] != false 
        ){
            return true;
        }
        else{
            return false;
        }
    }
}
Tetris.prototype.movementCollisionY = function( offset ){
    // Check if movement will be out of bound.
    if( 
        ( this.block1.position.y + offset ) < -22 || // block1 bottom bound check
        ( this.block2.position.y + offset ) < -22 || // block2 bottom bound check
        ( this.block3.position.y + offset ) < -22 || // block3 bottom bound check
        ( this.block4.position.y + offset ) < -22    // block4 bottom bound check
    ){
        return true;
    }
    else{
        if(
            gameGrid[ Math.abs( this.block1.position.y + offset ) ][ this.block1.position.x ] != false ||
            gameGrid[ Math.abs( this.block2.position.y + offset ) ][ this.block2.position.x ] != false ||
            gameGrid[ Math.abs( this.block3.position.y + offset ) ][ this.block3.position.x ] != false ||
            gameGrid[ Math.abs( this.block4.position.y + offset ) ][ this.block4.position.x ] != false
        ){
            return true;
        }
        else{
            return false;
        }
    }
}

Tetris.prototype.outOfBoundXLeft = function( posX ){
    if(
        ( this.block1.position.x + posX ) < 0  ||  // block1 Left bound check
        ( this.block2.position.x + posX ) < 0  ||  // block2 Left bound check
        ( this.block3.position.x + posX ) < 0  ||  // block3 Left bound check
        ( this.block4.position.x + posX ) < 0      // block4 Left bound check
    ) {
        return true;
    }
    else{
        return false;
    }
}
Tetris.prototype.outOfBoundXRight = function( posX ){
    if(
        ( this.block1.position.x + posX + 1 ) > 10 ||  // block1 Right bound check
        ( this.block2.position.x + posX + 1 ) > 10 ||  // block2 Right bound check
        ( this.block3.position.x + posX + 1 ) > 10 ||  // block3 Right bound check
        ( this.block4.position.x + posX + 1 ) > 10  // block4 Right bound check
    ) {
        return true;
    }
    else{
        return false;
    }
}
Tetris.prototype.setRotation = function( tetrimino, rotation ){
    //                                                    tetrimino    rotation  block x/y
    this.block1.position.x = this.posx + tetrisArray[ tetrimino ][ rotation ][ 0 ][ 0 ];
    this.block1.position.y = this.posy + tetrisArray[ tetrimino ][ rotation ][ 0 ][ 1 ];
    
    this.block2.position.x = this.posx + tetrisArray[ tetrimino ][ rotation ][ 1 ][ 0 ];
    this.block2.position.y = this.posy + tetrisArray[ tetrimino ][ rotation ][ 1 ][ 1 ];
    
    this.block3.position.x = this.posx + tetrisArray[ tetrimino ][ rotation ][ 2 ][ 0 ];
    this.block3.position.y = this.posy + tetrisArray[ tetrimino ][ rotation ][ 2 ][ 1 ];
    
    this.block4.position.x = this.posx + tetrisArray[ tetrimino ][ rotation ][ 3 ][ 0 ];
    this.block4.position.y = this.posy + tetrisArray[ tetrimino ][ rotation ][ 3 ][ 1 ];
}
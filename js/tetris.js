////////////////////////////////////////////////////////////////////////////////////////
//GLOBAL WEBGL VARIABLES
// div container for webgl
var divContainer = $( "#container" );
var widthMultiplier = 0.5;

// Canvas width/height
var width = divContainer.width();
var height = divContainer.height();

// Screen height size
var viewSize = 20.2;

// variables used to log keypress
var keys= {};

// scene, camera, renderer
var scene;
var camera;
var renderer;

////////////////////////////////////////////////////////////////////////////////////////
//GLOBAL GAME VARIABLES
// Game "grid"
// 0 - 2 is the spawn that's not visible to player
// 3 - 22 is game zone.
// y = Math.abs( posY );
// x = Math.abs( posX );
// gameGrid[ y ][ x ];
// Rectangle position is at bottom left.
// So technically cubes position.y starts from 1
var gameGrid = new Array( 23 );

// blockCount, 1 row, 2 row, 3 row, 4 row
var score = [ 0, 0, 0, 0, 0 ];
var tetriminoLog = [ 0, 0, 0, 0, 0, 0, 0, 0 ];

// Tetrimino
var tetrisMove;

// Variables for tetris. How long player needs to wait before tetris perform next move/rotation. ms
var tetrisMoveWaitX = 150;
var tetrisRotationWait = 250;
// How long tetris waits before next default y movement.
// Default y movement is dropping without player inputs
// GameSpeed also sets instant drop coolddown
var gameSpeed = 600;
// x time faster
var dropSpeedMultiplier = 2;
// When tetris cannot move down anymore it starts graceTimeCounter
// Tetris can still move x directions during this grace time
// When graceTimeCounter fills to graceTime tetris final position will be here.
var graceTime = 1000;

// COUNTERS
var gameSpeedCounter = 0;
var graceTimeCounter = 0;
var instantDropCounter = 0;

var lastTime;

////////////////////////////////////////////////////////////////////////////////////////
//FUNCTION FOR "GAME LOOP"
function animate() {
    //"game loop"
    requestAnimFrame( animate );
    //time stuffs
    var time = new Date().getTime();
    var deltaTime = ( time - lastTime );
    if( gameSpeed != 0){
        //update logic
        tetrisMove.updateLastMoveTime( deltaTime );
        
        // Update gameSpeedCounter
        gameSpeedCounter += deltaTime;
        instantDropCounter += deltaTime;
        
        // Checks if tetrimino can move down
        // If not then starts graceTimeCounter
        if( !( tetrisMove.movementCollisionY( -1 ) ) ){
            // Resets graceTimeCounter if tetris can move down
            graceTimeCounter = 0;
            // Check if enough time passed for default y movement
            if( gameSpeedCounter >= gameSpeed ){
                // Reset gameSpeedCounter
                // -= gameSpeed. This way it's more accurate.
                gameSpeedCounter -= gameSpeed;
                tetrisMove.moveYForce( -1 );
            }
        }
        else{
            graceTimeCounter += deltaTime;
        }
        
        // If gracetimeCounter is smaller than allowed graceTime
        // tetrimino can still move 
        if( graceTimeCounter < graceTime ){
            // A
            if( keys[ 65 ] == true ){
                tetrisMove.moveX( -1 );
            }
            // D
            if( keys[ 68 ] == true ){
                tetrisMove.moveX( 1 );
            }
            // W 
            if( keys[ 87 ] == true ){
                if( instantDropCounter > 300 ){
                    tetrisMove.instantDrop();
                    // No grace time on instant drop
                    graceTimeCounter = graceTime;
                    gameSpeedCounter = 0;
                    instantDropCounter = 0;
                }
            }
            // S 
            if( keys[ 83 ] == true ){
                // gameSpeedCounter fills faster
                gameSpeedCounter += ( dropSpeedMultiplier * deltaTime );
                // graceTimeCounter fills faster
                graceTimeCounter += 50;
            }
            // C
            if( keys[ 67 ] == true ) {
                graceTimeCounter -= tetrisMove.rotate( -1 );
            }
            // V
            if( keys[ 86 ] == true) {
                graceTimeCounter -= tetrisMove.rotate( 1 );
            }
        }
        // Tetrimino landed
        else{
            graceTimeCounter = 0;
            tetrisMove.endTetrisLifeHere();
            
            // Save tetrimino blocks y positions.
            var rows = [
                Math.abs( tetrisMove.block1.position.y ),
                Math.abs( tetrisMove.block2.position.y ),
                Math.abs( tetrisMove.block3.position.y ),
                Math.abs( tetrisMove.block4.position.y )
            ];
            // Sort rows by ascending order
            rows.sort( function( a, b ){ return a - b } );
            
            // Leave only unique rows
            rows = rows.filter( function( row, pos ){
                return rows.indexOf( row ) == pos;
            });
            
            var rowsCompleted = 0;
            
            rows.forEach( function( row ){
                var tempCounter = 0;
                // Check if this row is completed
                for( var x = 0; x < 10; x++ ){
                    if( gameGrid[ row ][ x ] != false ){ tempCounter++; }
                }
                if( tempCounter == 10 ){
                    rowsCompleted++;
                    // Remove mesh from this row.
                    for( var x = 0; x < 10; x++ ){
                        scene.remove( gameGrid[ row ][ x ] );
                    }
                    // Moves the rows below 2 down by 1
                    // Stops at 3. Row 2 - 0 is "spawn area" that's not visible to a player.
                    for( var y = row; y > 2; y-- ){
                        for(var x = 0; x < 10; x++){
                            gameGrid[ y ][ x ] = gameGrid[ y - 1 ][ x ];
                            if( gameGrid[ y ][ x ] != false ){
                                gameGrid[ y ][ x ].position.y -= 1;
                            }
                        }
                    }
                    // Empty row 2 which is copied by row 3
                    //for( var x = 0; x < 10; x++ ){ gameGrid[ 2 ][ x ] = false; }
                }
            });
            
            // TODO: Game over?
            for( var x = 0; x < 10; x++ ){
                if( gameGrid[ 2 ][ x ] != false ){
                    gameOver();
                    gameSpeed = 0;
                    break;
                }
            }
            
            if( rowsCompleted != 0 ){
                score[ rowsCompleted ]++;
                updateScoreDiv();
            }
            var tetriminoTemp = Math.floor(Math.random() * 7);
            tetriminoLog[ tetriminoTemp ]++;
            tetrisMove = new Tetris( tetriminoTemp, tetrisMoveWaitX, tetrisRotationWait );
            
        }
    }
    //draw all stuffs
    renderer.render( scene, camera );
    //update lastTime
    lastTime = time;
}

////////////////////////////////////////////////////////////////////////////////////////
//HTML ACTIONS
// Change some setting on load/resize

$( window ).bind( "load", function(){
    setContainerSize();
    
});

$( window ).bind( "resize", function(){
    setContainerSize();
    
    if( score[0] != 0 ){
        camera.updateProjectionMatrix();
        renderer.setSize( width, height );
    }
});
$( document ).on( "click", "#start", function( event ){
    // Auto focus #container for keyup / keydown
    if( !( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) ) {
        divContainer.attr( "tabindex", 0 );
        divContainer.focus();
    }
    
    // Initialize game variables
    initGame();
    
    // START RENDERING
    animate();
});
$( document ).on( "click", "#newGame", function( event ){
    // Remove gameover message
    $( "#gameover" ).remove();
    // Focus on container element for keyup / keydown
    if( !( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) ) {
        divContainer.focus();
    }
    // Reset game variables
    restartGame();
});
// To make a div element selectable. You require tabindex="x" in said div element
// Example here: <div id="container" tabindex="0"></div>
// then renderer .html canvas inside container
$( document ).on( "keydown", "#container", function ( event ){
    keys[ event.which ] = true;
});
$( document ).on( "keyup", "#container",function( event ){
    keys[ event.which ] = false;
});


// Detect mobile device
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    $( ".leftContainer" ).remove();
    $( ".rightContainer" ).remove();
    
    var instructionHtml = "" + 
        "<h3>Top is movement<h3>" + 
        "<h3>LEFT - RIGHT</h3>" +
        "<h3>Middle<h3>" + 
        "<h3>DOWN FASTER - INSTANT DROP</h3>" +
        "<h3>Top is rotation<h3>" + 
        "<h3>LEFT - RIGHT</h3>";
    $( "#instruction" ).html( instructionHtml );
    
    $( "#bodyContainer" ).append( 
        "<div id='tLeft' class='touchDiv' ></div>" + 
        "<div id='tRight' class='touchDiv' ></div>" + 
        "<div id='mLeft' class='touchDiv' ></div>" + 
        "<div id='mRight' class='touchDiv' ></div>" + 
        "<div id='bLeft' class='touchDiv' ></div>" + 
        "<div id='bRight' class='touchDiv' ></div>"
     );
    
    var aDiv = document.getElementById( "tLeft" );
    var dDiv = document.getElementById( "tRight" );
    var wDiv = document.getElementById( "mLeft" );
    var sDiv = document.getElementById( "mRight" );
    var cDiv = document.getElementById( "bLeft" );
    var vDiv = document.getElementById( "bRight" );
    
    // A
    aDiv.addEventListener( "touchstart", function( event ){
        event.preventDefault();
        keys[ 65 ] = true;
    }, false);
    aDiv.addEventListener( "touchend", function( event ){
        event.preventDefault();
        keys[ 65 ] = false;
    }, false);
    // D
    dDiv.addEventListener( "touchstart", function( event ){
        event.preventDefault();
        keys[ 68 ] = true;
    }, false);
    dDiv.addEventListener( "touchend", function( event ){
        event.preventDefault();
        keys[ 68 ] = false;
    }, false);
    // W
    wDiv.addEventListener( "touchstart", function( event ){
        event.preventDefault();
        keys[ 87 ] = true;
    }, false);
    wDiv.addEventListener( "touchend", function( event ){
        event.preventDefault();
        keys[ 87 ] = false;
    }, false);
    // S
    sDiv.addEventListener( "touchstart", function( event ){
        event.preventDefault();
        keys[ 83 ] = true;
    }, false);
    sDiv.addEventListener( "touchend", function( event ){
        event.preventDefault();
        keys[ 83 ] = false;
    }, false);
    // C
    cDiv.addEventListener( "touchstart", function( event ){
        event.preventDefault();
        keys[ 67 ] = true;
    }, false);
    cDiv.addEventListener( "touchend", function( event ){
        event.preventDefault();
        keys[ 67 ] = false;
    }, false);
    // V
    vDiv.addEventListener( "touchstart", function( event ){
        event.preventDefault();
        keys[ 86 ] = true;
    }, false);
    vDiv.addEventListener( "touchend", function( event ){
        event.preventDefault();
        keys[ 86 ] = false;
    }, false);
}
////////////////////////////////////////////////////////////////////////////////////////
//TEST SOME THINGS

/*
setInterval( function(){

}, 1000);
*/
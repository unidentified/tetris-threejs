////////////////////////////////////////////////////////////////////////////////////////
// ANIMATION LOOPING
// timeout 1000/60 seconds = ~16ms for 60fps
window.requestAnimFrame = (
    function(){
        return  window.requestAnimationFrame  ||
        window.webkitRequestAnimationFrame    ||
        window.mozRequestAnimationFrame       ||
        function( callback ){
            window.setTimeout(callback, 1000 / 60);
        };
    }
)();

// This sets size of canvasContainer
function setContainerSize(){
    height = window.innerHeight;
    
    divContainer.height( height );
    divContainer.width( height * widthMultiplier );
    
    width = divContainer.width();
}

// Initialize WebGL variables
function initWebGL(){
    renderer = new THREE.WebGLRenderer();
    renderer.setSize( width, height );
    renderer.setClearColor( 0xFFFFFF, 1 );

    // Camera
    camera = new THREE.OrthographicCamera(
        ( width / height * viewSize ) / -2,
        ( width / height * viewSize ) / 2,
        viewSize / 2,
        viewSize / -2,
        -1000, 1000 );
    
    // Set camera position
    camera.position.x = 5;
    camera.position.y = -12;
    camera.position.z = 10;

    scene = new THREE.Scene();
    scene.add( camera );
    // Insert canvas into #container
    if ( !window.WebGLRenderingContext ) {
        divContainer.html( "No support for WebGLRenderer" );
    }
    else {
        divContainer.html( renderer.domElement );
        /*
        var context = renderer.domElement.getContext( "webgl" );
        if ( !context ) {
            divContainer.html( "Support WebGLRenderer but failed to render" );
        }
        */
    }
}

function initGame(){
    // Generate and initialize gameGrid
    for( var y = 0; y < 23; y++ ){
        gameGrid[ y ] = new Array( 10 );
        for( var x = 0; x < 10; x++ ){
            gameGrid[ y ][ x ] = false;
        }
    }
    
    initWebGL();
    drawGameZone( 0.1, 0.1, 0.1 );
    
    var tetriminoTemp = Math.floor(Math.random() * 7);
    tetriminoLog[ tetriminoTemp ]++;
    tetrisMove = new Tetris( tetriminoTemp, tetrisMoveWaitX, tetrisRotationWait );

    lastTime = new Date().getTime();
}

function restartGame(){
    // Remove all blocks inside game zone
    for( var y = 0; y < 23; y++ ){
        for( var x = 0; x < 10; x++ ){
            if( gameGrid[ y ][ x ] != false ){
                scene.remove( gameGrid[ y ][ x ] );
            }
            gameGrid[ y ][ x ] = false;
        }
    }
    
    // Reset game speed and counters
    gameSpeed = 600;
    gameSpeedCounter = 0;
    graceTimeCounter = 0;
    instantDropCounter = 0;
    
    score = [ 0, 0, 0, 0, 0 ];
    tetriminoLog = [ 0, 0, 0, 0, 0, 0, 0, 0 ];
    
    var tetriminoTemp = Math.floor(Math.random() * 7);
    tetriminoLog[ tetriminoTemp ]++;
    tetrisMove = new Tetris( tetriminoTemp, tetrisMoveWaitX, tetrisRotationWait );
}



// Draws "box" as "game zone"
function drawGameZone( r, g, b ){
    var geometry = new THREE.Geometry();
    geometry.vertices.push( new THREE.Vector3( 0,   20, 0 ) );
    geometry.vertices.push( new THREE.Vector3( 10,  20, 0 ) );
    geometry.vertices.push( new THREE.Vector3( 10,  0,  0 ) );
    geometry.vertices.push( new THREE.Vector3( 0,   0,  0 ) );
    
    geometry.faces.push( new THREE.Face3( 0, 1, 2 ) );
    geometry.faces.push( new THREE.Face3( 0, 2, 3 ) );
    
    var material = new THREE.LineBasicMaterial( { color: new THREE.Color( r, g, b ), side: THREE.DoubleSide } );
    var mesh = new THREE.Mesh( geometry, material );
    
    mesh.position.set( 0, -22, 0 );
    
    scene.add( mesh );
}

// Create rectangle with given parameters
function createRectangle( name, width, height, posX, posY, r, g, b ){
    var geometry = new THREE.Geometry();
    //create 2x triangle (technically)
    geometry.vertices.push( new THREE.Vector3( 0,      height, 0 ) );
    geometry.vertices.push( new THREE.Vector3( width,  height, 0 ) );
    geometry.vertices.push( new THREE.Vector3( width,  0,      0 ) );
    geometry.vertices.push( new THREE.Vector3( 0,      0,      0 ) );
    
    //create faces for (technically) triangles using their vertices
    geometry.faces.push( new THREE.Face3( 0, 1, 2 ) );
    geometry.faces.push( new THREE.Face3( 0, 2, 3 ) );
    var material = new THREE.MeshBasicMaterial( { color: new THREE.Color( r, g, b ), side: THREE.DoubleSide } );
    
    var mesh = new THREE.Mesh( geometry, material );
    
    mesh.position.set( posX, posY, 0 );
    mesh.name = score[0]++;
    
    // New block added. So lets update score div
    updateScoreDiv();
    
    scene.add( mesh );
    
    return mesh;
}
function createRectangleTexture( posX, posY, tetrimino ){
    var geometry = new THREE.Geometry();
    // Create 2x triangle (technically)
    geometry.vertices.push( new THREE.Vector3( 0, 1, 0 ) );
    geometry.vertices.push( new THREE.Vector3( 1, 1, 0 ) );
    geometry.vertices.push( new THREE.Vector3( 1, 0, 0 ) );
    geometry.vertices.push( new THREE.Vector3( 0, 0, 0 ) );
    
    // Create faces for (technically) triangles using their vertices
    geometry.faces.push( new THREE.Face3( 0, 1, 2 ) );
    geometry.faces.push( new THREE.Face3( 0, 2, 3 ) );
    
    // Create UV coordinates for texture mapping
    geometry.faceVertexUvs[0].push([
        new THREE.Vector2( 0, 1 ),
        new THREE.Vector2( 1, 1 ),
        new THREE.Vector2( 1, 0 )
        ]);
    geometry.faceVertexUvs[0].push([
        new THREE.Vector2( 0, 1 ),
        new THREE.Vector2( 1, 0 ),
        new THREE.Vector2( 0, 0 )
        ]);
    
    var material = new THREE.MeshBasicMaterial( { map: tetrisArray[ tetrimino ][ 5 ], side: THREE.DoubleSide } );
    //var material = new THREE.MeshBasicMaterial( { map: tetrisArray[ tetrimino ][ 5 ] } );
    
    var mesh = new THREE.Mesh( geometry, material );
    
    mesh.position.set( posX, posY, 0 );
    mesh.name = score[0]++;
    
    // New block added. So lets update score div
    updateScoreDiv();
    
    scene.add( mesh );
    
    return mesh;
}

function updateScoreDiv(){
    var scoreHtml = "";
    scoreHtml += "blocks: " + score[ 0 ] + "<br />";
    scoreHtml += "1 rows: " + score[ 1 ] + "<br />";
    scoreHtml += "2 rows: " + score[ 2 ] + "<br />";
    scoreHtml += "3 rows: " + score[ 3 ] + "<br />";
    scoreHtml += "4 rows: " + score[ 4 ] + "<br />";
    
    scoreHtml += "L: " + tetriminoLog[ 0 ] + "<br />";
    scoreHtml += "J: " + tetriminoLog[ 1 ] + "<br />";
    scoreHtml += "O: " + tetriminoLog[ 2 ] + "<br />";
    scoreHtml += "S: " + tetriminoLog[ 3 ] + "<br />";
    scoreHtml += "Z: " + tetriminoLog[ 4 ] + "<br />";
    scoreHtml += "T: " + tetriminoLog[ 5 ] + "<br />";
    scoreHtml += "I: " + tetriminoLog[ 6 ] + "<br />";
    
    $( "#score" ).html( scoreHtml );
}
function gameOver(){
    var gameOverHtml = "";
    gameOverHtml += '<div id="gameover" style="display: none;">';
        gameOverHtml += ' Game Over! <br />';
        gameOverHtml += ' <button type="button" id="newGame" class="btn btn-primary" style="display: block; margin: auto; position:relative; "> New Game! </button>';
    gameOverHtml += '</div>';
    $( "#bodyContainer" ).append( gameOverHtml );
    
    var gameOverElement = $( "#gameover" );
    gameOverElement.width( width );
    gameOverElement.css( "top", Math.max( 0, ( ( $( window ).height() - gameOverElement.outerHeight() ) / 2 ) ) + "px" );
    gameOverElement.css( "left", Math.max( 0, ( ( $( window ).width() - gameOverElement.outerWidth() ) / 2 ) ) + "px" );
    
    $( "#gameover" ).fadeIn("slow");
}